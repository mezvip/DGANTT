package dganttomar;

import java.awt.Graphics;
import java.awt.geom.Rectangle2D;


public interface TaskRenderer {
	
	
	public void paintTask(Graphics g, GanttChart chart, Object task, 
			Rectangle2D bounds, boolean selected);

}