package dganttomar;


public abstract class LinkModel {
	
	
	public LinkModel() {
		super();
	}

	
	public abstract int getLinkCount();
	
	
	public abstract Link getLinkAt(int index);
	
	
	public void addLink(Link link) {
		
	}
	
	
	public void removeLink(Link link) {
		
	}
	
}