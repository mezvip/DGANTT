package dganttomar;

import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.geom.Point2D;
import java.text.Bidi;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JComponent;
import javax.swing.SwingUtilities;


public class TextUtilities {


	public static final int CENTER = SwingUtilities.CENTER;
	

	public static final int LEFT = SwingUtilities.LEFT;
	

	public static final int RIGHT = SwingUtilities.RIGHT;
	

	public static final int TOP = SwingUtilities.TOP;
	
	
	public static final int BOTTOM = SwingUtilities.BOTTOM;

	
	public static void paintString(Graphics g, String text, Point2D p) {
		g.drawString(text, (int)p.getX(), (int)p.getY() + 
				g.getFontMetrics().getAscent());
	}
	
	
	public static void paintString(Graphics g, String text, Rectangle bounds,
			int halign, int valign) {
		paintString(g, text, bounds, halign, valign, false);
	}

	
	public static void paintString(Graphics g, String text, Rectangle bounds, 
			int halign, int valign, boolean force) {
		FontMetrics fm = g.getFontMetrics();

		if (!force && (fm.getHeight() > bounds.getHeight())) {
			return;
		}

		if (fm.stringWidth(text) > bounds.getWidth()) {
			text = clipString(null, fm, text, (int)bounds.getWidth());
		}

		int hoffset = 0;
		switch (halign) {
		case LEFT:
			//do nothing
			break;
		case CENTER:
			hoffset = (bounds.width - fm.stringWidth(text))/2;
			break;
		case RIGHT:
			hoffset = bounds.width - fm.stringWidth(text);
			break;
		default:
			throw new IllegalStateException();
		}

		int voffset = 0;
		switch (valign) {
		case TOP:
			break;
		case CENTER:
			voffset = (bounds.height - fm.getHeight())/2;
			break;
		case BOTTOM:
			voffset = bounds.height - fm.getHeight();
			break;
		default:
			throw new IllegalStateException();
		}

		paintString(g, text, new Point(bounds.x+hoffset, bounds.y+voffset));
	}

	
	public static void paintText(Graphics g, String text, Rectangle bounds, 
			int halign, int valign) {   	
		Point pen = bounds.getLocation();
		Graphics2D g2 = (Graphics2D)g;
		FontMetrics fm = g2.getFontMetrics();
		List<String> lines = splitText(g, text, bounds);

		switch (valign) {
		case TOP:
			
			break;
		case CENTER:
			pen.y += (bounds.height - fm.getHeight()*lines.size())/2;
			break;
		case BOTTOM:
			pen.y += (bounds.height - fm.getHeight()*lines.size());
			break;
		default:
			throw new IllegalStateException();
		}

		for (int i=0; i<lines.size(); i++) {
			paintString(g, lines.get(i), new Rectangle(pen.x, pen.y, 
					bounds.width, fm.getHeight()), halign, TOP);
			pen.y += fm.getHeight();
		}
	}

	
	protected static List<String> splitText(Graphics g, String text, 
			Rectangle bounds) {
		Point pen = bounds.getLocation();
		Graphics2D g2 = (Graphics2D)g;
		FontMetrics fm = g2.getFontMetrics();
		ArrayList<String> result = new ArrayList<String>();
		int index = 0;
		
		while (index < text.length()) {
			int end = getLineBreakIndex(g, text, index, bounds.getWidth());
			
			if (end == index) {
				result.add("...");
				break;
			}
			
			String str = text.substring(index, end).trim();
			index = end;

			if ((pen.getY() + fm.getHeight() + fm.getHeight()) >
					(bounds.getY() + bounds.getHeight())) {
				if (end < text.length()) {
					str = clipString(null, fm, str, (int)bounds.getWidth());
				}
				
				result.add(str);
				break;
			}

			result.add(str);
			pen.y += fm.getHeight();
		}
		
		return result;
	}

	
	public static boolean isTextInBounds(Graphics g, String text, 
			Rectangle bounds) {
		Point pen = bounds.getLocation();
		Graphics2D g2 = (Graphics2D)g;
		FontMetrics fm = g2.getFontMetrics();
		int index = 0;
		
		while (index < text.length()) {
			int end = getLineBreakIndex(g, text, index, bounds.getWidth());
			
			if (end == index) {
				return false;
			}
			
			index = end;

			if ((pen.getY() + fm.getHeight() + fm.getHeight()) >
					(bounds.getY() + bounds.getHeight())) {
				if (index < text.length()) {
					return false;
				}
			}
			
			pen.y += fm.getHeight();
		}
		
		return true;
	}


	public static int findFontSize(Graphics g, String text, Rectangle bounds, 
			int minSize, int maxSize) {
		Font font = g.getFont();
		int size = findFontSizeBinarySearch(g, text, bounds, 
				(minSize+maxSize)/2, minSize, maxSize);
		g.setFont(font);
		return size;
	}

	
	private static int findFontSizeBinarySearch(Graphics g, String text, 
			Rectangle bounds, int size, int minSize, int maxSize) {
		if (maxSize - minSize <= 1) {
			return size;
		}

		Font font = new Font(g.getFont().getName(), g.getFont().getStyle(),
				size);
		g.setFont(font);
		
		if (isTextInBounds(g, text, bounds)) {
			return findFontSizeBinarySearch(g, text, bounds, (maxSize+size)/2, 
					size, maxSize);
		} else {
			return findFontSizeBinarySearch(g, text, bounds, (minSize+size)/2, 
					minSize, size);
		}
	}

	
	public static int getLineBreakIndex(Graphics g, String text, int index, 
			double maxWidth) {
		FontMetrics fm = g.getFontMetrics();
		int i = index;
		double width = 0.0;

		
		while (i < text.length()) {
			int codePoint = text.codePointAt(i);
			width += fm.charWidth(codePoint);
			
			if (width >= maxWidth) { 
				break;
			}
			
			i++;
		}

		if (i == text.length()) {
			return i;
		}

		
		while (i > index) {
			int codePoint = text.codePointAt(i);
			
			if (!Character.isLetterOrDigit(codePoint)) {
				break;
			}
			
			i--;
		}

		return i;
	}

	

	
	private static final int CHAR_BUFFER_SIZE = 100;
	private static final Object charsBufferLock = new Object();
	private static char[] charsBuffer = new char[CHAR_BUFFER_SIZE];

	private static final boolean isComplexLayout(char ch) {
		return (ch >= '\u0900' && ch <= '\u0D7F') || 
				(ch >= '\u0E00' && ch <= '\u0E7F') || 
				(ch >= '\u1780' && ch <= '\u17ff') || 
				(ch >= '\uD800' && ch <= '\uDFFF');   
	}

	private static final boolean isSimpleLayout(char ch) {
		return ch < 0x590 || (0x2E00 <= ch && ch < 0xD800);
	}

	public static final boolean isComplexLayout(char[] text, int start,
			int limit) {
		boolean simpleLayout = true;
		char ch;
		for (int i = start; i < limit; ++i) {
			ch = text[i];
			if (isComplexLayout(ch)) {
				return true;
			}
			if (simpleLayout) {
				simpleLayout = isSimpleLayout(ch);
			}
		}
		if (simpleLayout) {
			return false;
		}
		return Bidi.requiresBidi(text, start, limit);
	}

	public static String clipString(JComponent c, FontMetrics fm,
			String string, int availTextWidth) {
		
		String clipString = "...";
		int stringLength = string.length();
		availTextWidth -= fm.stringWidth(clipString);
		boolean needsTextLayout = false;

		synchronized (charsBufferLock) {
			if (charsBuffer == null || charsBuffer.length < stringLength) {
				charsBuffer  = string.toCharArray();
			} else {
				string.getChars(0, stringLength, charsBuffer, 0);
			}
			needsTextLayout = 
				isComplexLayout(charsBuffer, 0, stringLength);
			if (!needsTextLayout) {
				int width = 0;
				for (int nChars = 0; nChars < stringLength; nChars++) {
					width += fm.charWidth(charsBuffer[nChars]);
					if (width > availTextWidth) {
						string = string.substring(0, nChars);
						break;
					}
				}
			}
		}
		
		if (needsTextLayout) {
			throw new RuntimeException("does not support complex text layout");
		}
		
		if (string.isEmpty()) {
			return "";
		} else {
			return string + clipString;
		}
	}

}