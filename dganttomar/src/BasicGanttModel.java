package dganttomar;

import java.util.List;
public class BasicGanttModel extends GanttModel {
private List<?> tasks;
public BasicGanttModel(List<?> tasks) { 
    	super(); 
    	this.tasks = tasks;
    }

	@Override
	public int getRowCount() {
		return tasks.size();
	}

	@Override
	public Object getTaskAt(int index) {
		return tasks.get(index);
	}

	@Override
	public int getTaskCount() {
		return tasks.size();
	}

}