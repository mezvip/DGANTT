package dganttomar;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.swing.JPanel;


public class GanttHeader extends JPanel {
	
	private static final long serialVersionUID = 2729473542931877090L;
	

	private final GanttChart chart;


	public GanttHeader(GanttChart chart) {
		super();
		this.chart = chart;
	}
	
	@Override
	public Dimension getPreferredSize() {
		FontMetrics fm = getFontMetrics(getFont());
		return new Dimension(chart.getWidth(), 2*fm.getHeight());
	}
	
	@Override
	public void paintComponent(Graphics g) {
		Graphics2D g2 = (Graphics2D)g;
		FontMetrics fm = getFontMetrics(getFont());

		g2.setColor(getBackground());
		g2.fill(g2.getClipBounds());
		
		if (canRenderRow(g2, Calendar.HOUR_OF_DAY, new SimpleDateFormat("h a"))) {
			renderRow(g2, Calendar.DATE, new SimpleDateFormat("dd MMMM yyyy"), 0, fm.getHeight());
			g2.setColor(Color.GRAY);
			g2.draw(new Line2D.Double(0, fm.getHeight(), getWidth(), fm.getHeight()));
			renderRow(g2, Calendar.HOUR_OF_DAY, new SimpleDateFormat("h a"), fm.getHeight(), fm.getHeight());			
		} else if (canRenderRow(g2, Calendar.DATE, new SimpleDateFormat("dd"))) {
			renderRow(g2, Calendar.MONTH, new SimpleDateFormat("MMM yyyy"), 0, fm.getHeight());
			g2.setColor(Color.GRAY);
			g2.draw(new Line2D.Double(0, fm.getHeight(), getWidth(), fm.getHeight()));
			renderRow(g2, Calendar.DATE, new SimpleDateFormat("dd"), fm.getHeight(), fm.getHeight());
		} else if (canRenderRow(g2, Calendar.WEEK_OF_YEAR, new SimpleDateFormat("'Week' w"))) {
			renderRow(g2, Calendar.MONTH, new SimpleDateFormat("MMM yyyy"), 0, fm.getHeight());
			g2.setColor(Color.GRAY);
			g2.draw(new Line2D.Double(0, fm.getHeight(), getWidth(), fm.getHeight()));
			renderRow(g2, Calendar.WEEK_OF_YEAR, new SimpleDateFormat("'Week' w"), fm.getHeight(), fm.getHeight());
		} else if (canRenderRow(g2, Calendar.MONTH, new SimpleDateFormat("MMM"))) {
			renderRow(g2, Calendar.YEAR, new SimpleDateFormat("yyyy"), 0, fm.getHeight());
			g2.setColor(Color.GRAY);
			g2.draw(new Line2D.Double(0, fm.getHeight(), getWidth(), fm.getHeight()));
			renderRow(g2, Calendar.MONTH, new SimpleDateFormat("MMM"), fm.getHeight(), fm.getHeight());
		} else {
			renderRow(g2, Calendar.YEAR, new SimpleDateFormat("yyyy"), 0, 2.0*fm.getHeight());
		}

		g2.setColor(Color.GRAY);
		g2.draw(new Line2D.Double(0, getHeight()-1, getWidth(), getHeight()-1));
	}
	

	private Calendar normalizeCalendar(Calendar calendar, int stepType) {
		Calendar result = Calendar.getInstance();
		
		result.clear();
		
		if (stepType == Calendar.YEAR) {
			result.set(Calendar.YEAR, calendar.get(Calendar.YEAR));
		} else if (stepType == Calendar.MONTH) {
			result.set(Calendar.YEAR, calendar.get(Calendar.YEAR));
			result.set(Calendar.MONTH, calendar.get(Calendar.MONTH));
		} else if (stepType == Calendar.WEEK_OF_YEAR) {
			result.set(Calendar.YEAR, calendar.get(Calendar.YEAR));
			result.set(Calendar.WEEK_OF_YEAR, calendar.get(Calendar.WEEK_OF_YEAR));	
		} else if (stepType == Calendar.DATE) {
			result.set(Calendar.YEAR, calendar.get(Calendar.YEAR));
			result.set(Calendar.MONTH, calendar.get(Calendar.MONTH));
			result.set(Calendar.DATE, calendar.get(Calendar.DATE));
		} else if (stepType == Calendar.HOUR_OF_DAY) {
			result.set(Calendar.YEAR, calendar.get(Calendar.YEAR));
			result.set(Calendar.MONTH, calendar.get(Calendar.MONTH));
			result.set(Calendar.DATE, calendar.get(Calendar.DATE));
			result.set(Calendar.HOUR_OF_DAY, calendar.get(Calendar.HOUR_OF_DAY));
		} else {
			throw new IllegalStateException("unsupported stepType");
		}
		
		return result;
	}
	

	private void renderRow(Graphics g, int stepType, DateFormat dateFormat, 
			double y, double height) {
		Graphics2D g2 = (Graphics2D)g;
		Rectangle2D bounds = g2.getClipBounds();
		long minimum = chart.screenToCanonical(bounds.getMinX());
		long maximum = chart.screenToCanonical(bounds.getMaxX());
		
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(minimum);
		calendar = normalizeCalendar(calendar, stepType);
		
		double start = chart.canonicalToScreen(calendar.getTimeInMillis());
		double end = 0.0;
		String text = null;
		
		while (calendar.getTimeInMillis() <= maximum) {
			text = dateFormat.format(calendar.getTime());
			calendar.add(stepType, 1);
			end = chart.canonicalToScreen(calendar.getTimeInMillis());

			g2.setColor(Color.GRAY);
			g2.draw(new Line2D.Double(start, y, start, y+height));
			g2.setColor(Color.BLACK);
			TextUtilities.paintString(g2, text, new Rectangle2D.Double(start, y,
					end-start, height).getBounds(), TextUtilities.CENTER, 
					TextUtilities.CENTER, true);
			
			start = end;
		}
		
		text = dateFormat.format(calendar.getTime());
		calendar.add(stepType, 1);
		end = chart.canonicalToScreen(calendar.getTimeInMillis());

		g2.setColor(Color.GRAY);
		g2.draw(new Line2D.Double(start, y, start, y+height));
		g2.setColor(Color.BLACK);
		TextUtilities.paintString(g2, text, new Rectangle2D.Double(start, y, 
				end-start, height).getBounds(), TextUtilities.CENTER, 
				TextUtilities.CENTER, true);
	}
	

	private boolean canRenderRow(Graphics g, int stepType, 
			DateFormat dateFormat) {
		Graphics2D g2 = (Graphics2D)g;
		FontMetrics fm = g2.getFontMetrics();

		Rectangle2D bounds = g2.getClipBounds();
		long minimum = chart.screenToCanonical(bounds.getMinX());
		long maximum = chart.screenToCanonical(bounds.getMaxX());
		
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(minimum);
		calendar = normalizeCalendar(calendar, stepType);
		
		double start = chart.canonicalToScreen(calendar.getTimeInMillis());
		double end = 0.0;
		String text = null;
		
		while (calendar.getTimeInMillis() <= maximum) {
			text = dateFormat.format(calendar.getTime());
			calendar.add(stepType, 1);
			end = chart.canonicalToScreen(calendar.getTimeInMillis());

			if (end - start < fm.stringWidth(text)) {
				return false;
			}
			
			start = end;
		}
		
		text = dateFormat.format(calendar.getTime());
		calendar.add(stepType, 1);
		end = chart.canonicalToScreen(calendar.getTimeInMillis());

		if (end - start < fm.stringWidth(text)) {
			return false;
		}
		
		return true;
	}

}