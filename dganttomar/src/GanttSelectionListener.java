package dganttomar;

import java.util.EventListener;


public interface GanttSelectionListener extends EventListener {
	
	
	public void valueChanged(GanttSelectionEvent e);

}