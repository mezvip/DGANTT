package dganttomar;

import java.awt.Cursor;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.Rectangle2D;


public class BasicTaskEditorHandler extends MouseAdapter {
	
	
	protected Point lastPoint;
	
	
	protected int mode;
	
	
	protected GanttChart chart;
	
	
	public static final int NONE = 0;
	
	
	public static final int RESIZE_START = 1;
	
	
	public static final int MOVE = 2;
	
	
	public static final int RESIZE_END = 3;
	
	
	public static final int MULTIPLE = 4;
	
	
	public BasicTaskEditorHandler(GanttChart chart) {
		super();
		this.chart = chart;
	}
	
	@Override
	public void mousePressed(MouseEvent e) {
		if (e.isConsumed()) {
			return;
		}
		
		if (e.getButton() != MouseEvent.BUTTON1) {
			return;
		}
		
		Object task = chart.getTaskAtPoint(e.getPoint());
		
		if (task == null) {
			if (!e.isControlDown()) {
				chart.clearSelection();
			}
		} else {
			if (e.isControlDown()) {
				chart.toggleTaskSelection(task);
			} else if (!chart.isTaskSelected(task)) {
				chart.clearSelection();
				chart.selectTask(task);
			}
		}
		
		chart.repaint(chart.getVisibleRect());
		
		if (e.isControlDown()) {
			return;
		}
		
		mode = getMode(e);
		
		if (mode == NONE) {
			return;
		}
		
		lastPoint = e.getPoint();
		
		e.consume();
	}
	
	@Override
	public void mouseReleased(MouseEvent e) {
		if (e.isConsumed()) {
			return;
		}
		
		lastPoint = null;
		chart.repaint(chart.getVisibleRect());
		
		e.consume();
	}
	
	
	protected int getMode(MouseEvent e) {
		if (chart.getSelectedTasks().isEmpty()) {
			return NONE;
		} else if (chart.getSelectedTasks().size() > 1) {
			return MULTIPLE;
		} else {
			return getMode(e, chart.getTaskAtPoint(e.getPoint()));
		}
	}
	
	
	protected int getMode(MouseEvent e, Object task) {		
		if (Math.abs(e.getPoint().getX() - chart.canonicalToScreen(
				chart.getTranslator().getStart(task))) <= 1.0) {
			return RESIZE_START;
		} else if (Math.abs(e.getPoint().getX() - chart.canonicalToScreen(
				chart.getTranslator().getEnd(task))) <= 1.0) {
			return RESIZE_END;
		} else {
			return MOVE;
		}
	}
	
	@Override
	public void mouseMoved(MouseEvent e) {
		if (e.isConsumed()) {
			return;
		}
		
		Object task = chart.getTaskAtPoint(e.getPoint());
		
		if (task == null) {
			chart.setCursor(Cursor.getDefaultCursor());
		} else {
			int mode = getMode(e, task);
			
			if (mode == RESIZE_START) {
				chart.setCursor(Cursor.getPredefinedCursor(Cursor.W_RESIZE_CURSOR));
			} else if (mode == RESIZE_END) {
				chart.setCursor(Cursor.getPredefinedCursor(Cursor.E_RESIZE_CURSOR));
			} else if (mode == MOVE) {
				chart.setCursor(Cursor.getPredefinedCursor(Cursor.MOVE_CURSOR));
			} else {
				throw new RuntimeException("undefined mode");
			}
			
			e.consume();
		}
	}
	
	@Override
	public void mouseDragged(MouseEvent e) {
		if (e.isConsumed()) {
			return;
		}
		
		if (lastPoint == null) {
			return;
		}
		
		if (chart.getSelectedTasks().isEmpty()) {
			return;
		}

		Point point = e.getPoint();
		double dx = point.getX() - lastPoint.getX();
		
		for (Object task : chart.getSelectedTasks()) {
			Rectangle2D bounds = chart.getTaskBounds(task);
			
			if (mode == MOVE) {
				Integer destinationRow = chart.getRow(point.getY());
				
				if (destinationRow != null) {
					chart.getTranslator().setRow(task, destinationRow);
				}
			}
			
			if (mode == RESIZE_START) {
				if (bounds.getWidth() - dx <= 0) {
					dx = bounds.getWidth();
				}
				
				bounds.setFrame(bounds.getX()+dx, bounds.getY(), 
						bounds.getWidth()-dx, bounds.getHeight());
			} else if (mode == RESIZE_END) {
				if (bounds.getWidth() + dx <= 0) {
					dx = -bounds.getWidth();
				}
				
				bounds.setFrame(bounds.getX(), bounds.getY(), 
						bounds.getWidth()+dx, bounds.getHeight());
			} else if ((mode == MOVE) || (mode == MULTIPLE)) {
				bounds.setFrame(bounds.getX()+dx, bounds.getY(), 
						bounds.getWidth(), bounds.getHeight());
			}
			
			
			if ((bounds.getMinX() > chart.getVisibleRect().getMinX()) ||
					(bounds.getMaxX() < chart.getVisibleRect().getMaxX())) {
				chart.scrollRectToVisible(bounds.getBounds());
			}
			
			chart.getTranslator().setStart(task, 
					chart.screenToCanonical(bounds.getMinX()));
			chart.getTranslator().setEnd(task, 
					chart.screenToCanonical(bounds.getMaxX()));
		}

		chart.repaint();
		chart.fireChangeEvent();
		
		lastPoint = point;
		e.consume();
	}
	
}