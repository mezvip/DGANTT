package dganttomar;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Rectangle;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JViewport;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;


public class GanttPanel extends JPanel {
	
	private static final long serialVersionUID = -1542893401367609772L;
	
	
	private final GanttChart chart;
	
	
	private final GanttHeader header;
	
	
	private final GanttTable table;
	
	
	public GanttPanel(GanttChart chart, GanttHeader header, GanttTable table) {
		super();
		this.chart = chart;
		this.header = header;
		this.table = table;
		
		layoutComponents();
	}
	
	
	private void layoutComponents() {
		final JScrollPane tableScrollPane = new JScrollPane(table, 
				JScrollPane.VERTICAL_SCROLLBAR_NEVER, 
				JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		tableScrollPane.setBorder(BorderFactory.createEmptyBorder());
		
		JPanel leftPanel = new JPanel(new BorderLayout());
		leftPanel.add(tableScrollPane, BorderLayout.CENTER);

		final JScrollPane chartScrollPane = new JScrollPane(chart, 
				JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, 
				JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		chartScrollPane.setBorder(BorderFactory.createEmptyBorder());
		chartScrollPane.setColumnHeaderView(header);
		
		JPanel rightPanel = new JPanel(new BorderLayout());
		rightPanel.add(chartScrollPane, BorderLayout.CENTER);
		
		JSplitPane splitPane = new JSplitPane();
		splitPane.setResizeWeight(0);
		splitPane.setLeftComponent(leftPanel);
		splitPane.setRightComponent(rightPanel);
		splitPane.setDividerLocation(100);
		
		JPanel upperRightCorner = new JPanel();
		upperRightCorner.setBorder(BorderFactory.createMatteBorder(0, 1, 0, 0, 
				Color.GRAY));
		chartScrollPane.setCorner(JScrollPane.UPPER_RIGHT_CORNER, 
				upperRightCorner);
		
		
		chartScrollPane.getViewport().addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				boolean visible = chart.getWidth() > chartScrollPane
						.getViewport().getWidth();
				visible |= table.getWidth() > tableScrollPane
						.getViewport().getWidth();
				int policy = visible ? JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS
						: JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED;
				tableScrollPane.setHorizontalScrollBarPolicy(policy);
				chartScrollPane.setHorizontalScrollBarPolicy(policy);
			}
		});
		
		tableScrollPane.getViewport().addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				boolean visible = chart.getWidth() > chartScrollPane
						.getViewport().getWidth();
				visible |= table.getWidth() > tableScrollPane
						.getViewport().getWidth();
				int policy = visible ? JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS
						: JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED;
				tableScrollPane.setHorizontalScrollBarPolicy(policy);
				chartScrollPane.setHorizontalScrollBarPolicy(policy);
			}
		});
		
		
		chartScrollPane.getVerticalScrollBar().addAdjustmentListener(
				new AdjustmentListener() {
					
			public void adjustmentValueChanged(AdjustmentEvent e) {
				Rectangle r = chart.getVisibleRect();
				r.x = 0;
				r.width = table.getWidth();
				table.scrollRectToVisible(r);
			}
			
		});
		
		
		tableScrollPane.getViewport().setScrollMode(
				JViewport.BACKINGSTORE_SCROLL_MODE);
		chartScrollPane.getColumnHeader().setScrollMode(
				JViewport.BACKINGSTORE_SCROLL_MODE);
		chartScrollPane.getViewport().setScrollMode(
				JViewport.BACKINGSTORE_SCROLL_MODE);
		
		setLayout(new BorderLayout());
		add(splitPane, BorderLayout.CENTER);
	}

}