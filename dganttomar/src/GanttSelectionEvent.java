package dganttomar;

import java.util.EventObject;

public class GanttSelectionEvent extends EventObject {
	
	private static final long serialVersionUID = 8622965228178188295L;

	
	public GanttSelectionEvent(GanttChart source)  {
		super(source);
	}

	@Override
	public GanttChart getSource() {
		return (GanttChart)super.getSource();
	}

}