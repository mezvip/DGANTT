package dganttomar;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;


public class BasicTaskRenderer implements TaskRenderer {
	
	
	public static final Color FOREGROUND = Color.BLACK;
	
	
	public static final Color BACKGROUND = new Color(127, 127, 255);
	
	
	public BasicTaskRenderer() {
		super();
	}

	@Override
	public void paintTask(Graphics g, GanttChart chart, Object task,
			Rectangle2D bounds, boolean selected) {
		Graphics2D g2 = (Graphics2D)g;

		g2.setColor(BACKGROUND);
		g2.fill(bounds);
		
		g2.setColor(Color.BLACK);
		g2.draw(bounds);
		
		if (selected) {
			g2.setColor(Color.RED);
			g2.draw(bounds);
		}
		
		g2.setColor(FOREGROUND);
		TextUtilities.paintString(g2, chart.getTranslator().getText(task), 
				bounds.getBounds(), TextUtilities.CENTER, TextUtilities.CENTER);
	}

}