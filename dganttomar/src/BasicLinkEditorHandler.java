package dganttomar;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;
public class BasicLinkEditorHandler extends MouseAdapter {
private Object selectedTask;
private Point selectedPoint;
private Point lastPoint;
private Object targetTask;
private Rectangle2D targetBounds;
private final GanttChart chart;
public BasicLinkEditorHandler(GanttChart chart) {
		super();
		this.chart = chart;
	}
	
	@Override
	public void mousePressed(MouseEvent e) {
		if (e.isConsumed()) {
			return;
		}
		
		if (e.getButton() != MouseEvent.BUTTON3) {
			return;
		}
		
		selectedPoint = e.getPoint();
		selectedTask = chart.getTaskAtPoint(selectedPoint);
		
		if (selectedTask == null) {
			selectedPoint = null;
			return;
		}
		
		lastPoint = selectedPoint;
		
		Graphics2D g2 = (Graphics2D)chart.getGraphics();
		g2.setXORMode(Color.WHITE);
		g2.draw(new Line2D.Double(selectedPoint, lastPoint));
		g2.fill(new Rectangle(selectedPoint.x-2, selectedPoint.y-2, 5, 5));
		g2.setPaintMode();
		
		chart.setCursor(Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR));
		
		e.consume();
	}
	
	@Override
	public void mouseReleased(MouseEvent e) {
		if (e.isConsumed() || (selectedPoint == null)) {
			return;
		}
		
		Graphics2D g2 = (Graphics2D)chart.getGraphics();
		g2.setXORMode(Color.WHITE);
		
		if (targetBounds != null) {
			g2.draw(targetBounds);
		}
		
		g2.draw(new Line2D.Double(selectedPoint, lastPoint));
		g2.fill(new Rectangle(selectedPoint.x-2, selectedPoint.y-2, 5, 5));
		g2.setPaintMode();
		
		chart.setCursor(Cursor.getDefaultCursor());
		
		lastPoint = e.getPoint();
		Object hoverTask = chart.getTaskAtPoint(lastPoint);
		
		if (hoverTask != null) {
			chart.getLinkModel().addLink(new Link(selectedTask, hoverTask, 
					LinkType.FINISH_TO_START));
			chart.repaint(chart.getVisibleRect());
		}
		
		selectedPoint = null;
		selectedTask = null;
		lastPoint = null;
		targetTask = null;
		targetBounds = null;
		
		e.consume();
	}
	
	@Override
	public void mouseEntered(MouseEvent e) {
		if (e.isConsumed() || (selectedTask == null)) {
			return;
		}
		
		e.consume();
	}
	
	@Override
	public void mouseExited(MouseEvent e) {
		if (e.isConsumed() || (selectedTask == null)) {
			return;
		}
		
		e.consume();
	}
	
	@Override
	public void mouseClicked(MouseEvent e) {
		if (e.isConsumed() || (selectedTask == null)) {
			return;
		}
		
		e.consume();
	}
	
	@Override
	public void mouseMoved(MouseEvent e) {
		if (e.isConsumed() || (selectedTask == null)) {
			return;
		}

		e.consume();
	}
	
	@Override
	public void mouseDragged(MouseEvent e) {
		if (e.isConsumed() || (selectedTask == null)) {
			return;
		}

		Graphics2D g2 = (Graphics2D)chart.getGraphics();
		g2.setXORMode(Color.WHITE);
		g2.draw(new Line2D.Double(selectedPoint, lastPoint));

		lastPoint = e.getPoint();
		Object hoverTask = chart.getTaskAtPoint(lastPoint);
		
		if ((hoverTask == null) || (hoverTask == selectedTask)) {
			if (targetBounds != null) {
				g2.draw(targetBounds);
			}
			
			targetBounds = null;
		} else if (hoverTask != targetTask) {
			if (targetBounds != null) {
				g2.draw(targetBounds);
			}
			
			targetTask = hoverTask;
			targetBounds = chart.getTaskBounds(targetTask, 
					chart.getRow(lastPoint.getY()), 0, 0);
			targetBounds.setRect(targetBounds.getX()-1.0, 
					targetBounds.getY()-1.0, targetBounds.getWidth()+2.0, 
					targetBounds.getHeight()+2.0);
			g2.draw(targetBounds);
		}
		
		g2.draw(new Line2D.Double(selectedPoint, lastPoint));
		g2.setPaintMode();
		
		e.consume();
	}
	
}