package dganttomar;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Polygon;
import java.awt.Shape;
import java.awt.geom.Rectangle2D;


public class BasicLinkRenderer implements LinkRenderer {
	
	
	public static final Color BACKGROUND = Color.RED;
	

	public BasicLinkRenderer() {
		super();
	}

	@Override
	public void paintLink(Graphics g, GanttChart chart, Link link) {
		Graphics2D g2 = (Graphics2D)g;
		Rectangle2D bounds1 = chart.getTaskBounds(link.getFirst());
		Rectangle2D bounds2 = chart.getTaskBounds(link.getSecond());
		Shape arrow = null;
		
		switch (link.getType()) {
		case START_TO_START:
			arrow = getArrow((int)bounds1.getMinX(), (int)bounds1.getCenterY(), 
					(int)bounds2.getMinX(), (int)bounds2.getCenterY());
			break;
		case FINISH_TO_START:
			arrow = getArrow((int)bounds1.getMaxX(), (int)bounds1.getCenterY(), 
					(int)bounds2.getMinX(), (int)bounds2.getCenterY());
			break;
		case FINISH_TO_FINISH:
			arrow = getArrow((int)bounds1.getMaxX(), (int)bounds1.getCenterY(), 
					(int)bounds2.getMaxX(), (int)bounds2.getCenterY());
			break;
		default:
			throw new IllegalStateException();
		}
		
		g2.setColor(BACKGROUND);
		g2.fill(arrow);
		g2.setColor(Color.BLACK);
		g2.draw(arrow);
	}


	private Polygon getArrow(int xCenter, int yCenter, int x, int y) {
		double aDir=Math.atan2(xCenter-x,yCenter-y);
		
		if ((xCenter-x == 0) && (yCenter-y == 0)) {
			
			aDir = -1.5707963267948966; 
		}
		
		Polygon tmpPoly=new Polygon();
		tmpPoly.addPoint(xCenter, yCenter);
		int i1=9;
		int i2=6;
		tmpPoly.addPoint(x+xCor(i2,aDir),y+yCor(i2,aDir));
		tmpPoly.addPoint(x+xCor(i1,aDir+.5),y+yCor(i1,aDir+.5));
		tmpPoly.addPoint(x,y);							
		tmpPoly.addPoint(x+xCor(i1,aDir-.5),y+yCor(i1,aDir-.5));
		tmpPoly.addPoint(x+xCor(i2,aDir),y+yCor(i2,aDir));
		return tmpPoly;
	}

	private static int yCor(int len, double dir) {
		return (int)(len * Math.cos(dir));
	}
	
	private static int xCor(int len, double dir) {
		return (int)(len * Math.sin(dir));
	}


}