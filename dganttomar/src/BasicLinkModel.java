package dganttomar;

import java.util.ArrayList;
import java.util.List;


public class BasicLinkModel extends LinkModel {

	
	private List<Link> links;
	
	
	public BasicLinkModel() {
		links = new ArrayList<Link>();
	}
	
	@Override
	public int getLinkCount() {
		return links.size();
	}
	
	@Override
	public Link getLinkAt(int index) {
		return links.get(index);
	}
	
	@Override
	public void addLink(Link link) {
		links.add(link);
	}
	
	@Override
	public void removeLink(Link link) {
		links.remove(link);
	}
	
	
	public void addLink(Object from, Object to, LinkType type) {
		addLink(new Link(from, to, type));
	}
	
}