package dganttomar;

import java.util.List;
import java.util.Vector;


public abstract class GanttModel {
	
	
	private final List<GanttModelListener> listeners;
	
	
	public GanttModel() {
		super();
		
		listeners = new Vector<GanttModelListener>();
	}
	
	
	public abstract int getRowCount();
	
	
	public abstract int getTaskCount();
	
	
	public abstract Object getTaskAt(int index);

	
	public void addGanttModelListener(GanttModelListener listener) {
		listeners.add(listener);
	}
	
	
	public void removeGanttModelListener(GanttModelListener listener) {
		listeners.remove(listener);
	}
	
	
	public void fireGanttModelChanged() {
		fireGanttModelChanged(new GanttModelEvent(this));
	}
	
	
	public void fireGanttModelChanged(int row) {
		fireGanttModelChanged(new GanttModelEvent(this, row));
	}
	
	
	public void fireGanttModelChanged(int firstRow, int lastRow) {
		fireGanttModelChanged(new GanttModelEvent(this, firstRow, lastRow));
	}
	
	
	public void fireGanttModelChanged(GanttModelEvent event) {
		for (GanttModelListener listener : listeners) {
			listener.ganttModelChanged(event);
		}
	}
	
	
	public void addTask(Object task) {
		//do nothing, default mode is read-only
	}
	
	
	public void removeTask(Object task) {
		
	}

}