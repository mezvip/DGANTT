package dganttomar;

import java.util.EventObject;


public class GanttModelEvent extends EventObject {

	private static final long serialVersionUID = -4746264910686760132L;

	
	private final int firstRow;
	
	
	private final int lastRow;
	
	
	public GanttModelEvent(GanttModel model) {
		this(model, 0, model.getRowCount());
	}
	
	
	public GanttModelEvent(GanttModel model, int row) {
		this(model, row, row);
	}
	
	
	public GanttModelEvent(GanttModel model, int firstRow, int lastRow) {
		super(model);
		this.firstRow = firstRow;
		this.lastRow = lastRow;
	}

	@Override
	public GanttModel getSource() {
		return (GanttModel)super.getSource();
	}

	
	public int getFirstRow() {
		return firstRow;
	}

	
	public int getLastRow() {
		return lastRow;
	}

}