package dganttomar;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Stroke;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.Rectangle2D;


public class BasicSelectionHandler extends MouseAdapter {
	
	
	public static final Stroke BoxStroke = new BasicStroke(1.0f, 
			BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER, 1.0f, 
			new float[] { 4.0f }, 0.0f);
	
	
	private final GanttChart chart;
	
	
	private Point startPoint;
	
	
	private Rectangle2D lastBox;

	
	public BasicSelectionHandler(GanttChart chart) {
		super();
		this.chart = chart;
	}

	@Override
	public void mousePressed(MouseEvent e) {
		if (e.isConsumed()) {
			return;
		}
		
		if (e.isControlDown()) {
			return;
		}
		
		if ((e.getButton() == MouseEvent.BUTTON1) && (e.getClickCount() == 1) && 
		    	(chart.getTaskAtPoint(e.getPoint()) == null)) {
			startPoint = e.getPoint();
			e.consume();
		}
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		if (e.isConsumed() || (startPoint == null)) {
			return;
		}
		
		Graphics2D g2 = (Graphics2D)chart.getGraphics();
		g2.setXORMode(Color.WHITE);
		g2.setStroke(BoxStroke);
		
		chart.getSelectedTasks().clear();
		
		if (lastBox != null) {
			g2.draw(lastBox);
			
			for (int i=0; i<chart.getModel().getTaskCount(); i++) {
				Object task = chart.getModel().getTaskAt(i);
				Rectangle2D bounds = chart.getTaskBounds(task);
				
				if (bounds.intersects(lastBox)) {
					chart.getSelectedTasks().add(task);
				}
			}
		}
		
		startPoint = null;
		lastBox = null;
		
		chart.repaint();
		chart.fireSelectionEvent();
		
		e.consume();
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		if (e.isConsumed() || (startPoint == null)) {
			return;
		}
		
		Point endPoint = e.getPoint();
		Graphics2D g2 = (Graphics2D)chart.getGraphics();
		g2.setXORMode(Color.WHITE);
		g2.setStroke(BoxStroke);
		
		if (lastBox != null) {
			g2.draw(lastBox);
		}
		
		lastBox = new Rectangle2D.Double(Math.min(startPoint.getX(), 
				endPoint.getX()),
				Math.min(startPoint.getY(), endPoint.getY()),
				Math.abs(endPoint.getX() - startPoint.getX()), 
				Math.abs(endPoint.getY() - startPoint.getY()));
		g2.draw(lastBox);
		
		e.consume();
	}
	
}