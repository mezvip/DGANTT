package dganttomar;


public class State {
	

	private final Object task;
	

	private final long start;
	

	private final long end;
	

	private final int row;
	

	public State(Object task, long start, long end, int row) {
		super();
		this.task = task;
		this.start = start;
		this.end = end;
		this.row = row;
	}


	public Object getTask() {
		return task;
	}


	public long getStart() {
		return start;
	}


	public long getEnd() {
		return end;
	}


	public int getRow() {
		return row;
	}
	
}