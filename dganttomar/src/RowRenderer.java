package dganttomar;

import java.awt.Graphics;
import java.awt.geom.Rectangle2D;


public interface RowRenderer {

	
	public void paintBackground(Graphics g, GanttChart chart);
	
	
	public void paintRow(Graphics g, GanttChart chart, int row,
			Rectangle2D bounds, boolean isTable);
	
}