package dganttomar;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.geom.Rectangle2D;


public class BasicRowRenderer implements RowRenderer {
	
	
	public static final Color ODD_COLOR = new Color(240, 240, 240);
	
	
	public static final Color EVEN_COLOR = Color.WHITE;
	
	
	public BasicRowRenderer() {
		super();
	}

	@Override
	public void paintBackground(Graphics g, GanttChart chart) {
		Graphics2D g2 = (Graphics2D)g;
		Rectangle clip = g2.getClipBounds();
		
		g2.setColor(chart.getBackground());
		g2.fill(clip);
	}

	@Override
	public void paintRow(Graphics g, GanttChart chart, int row,
			Rectangle2D bounds, boolean isTable) {
		Graphics2D g2 = (Graphics2D)g;
		
		if (row % 2 == 1) {
			g2.setColor(ODD_COLOR);
		} else {
			g2.setColor(EVEN_COLOR);
		}
		
		g2.fill(bounds);
	}

}