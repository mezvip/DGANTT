package dganttomar;

import java.awt.Graphics;


public interface LinkRenderer {


	public void paintLink(Graphics g, GanttChart chart, Link link);
	
}