package dganttomar;

import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.Rectangle2D;

import javax.swing.JTextField;
public class BasicDoubleClickHandler extends MouseAdapter 
implements ActionListener, FocusListener, ComponentListener {
	private final GanttChart chart;
	private Object task;
	private JTextField editor;
public BasicDoubleClickHandler(GanttChart chart) {
		super();
		this.chart = chart;
	}
public void startEdit(Object task) {
		this.task = task;
		
		int row = chart.getTranslator().getRow(task);
		Rectangle2D bounds = chart.getTaskBounds(task, row, 0, 0);
		
		editor = new JTextField(chart.getTranslator().getText(task));
		editor.addActionListener(this);
		editor.addFocusListener(this);
		editor.setBounds(bounds.getBounds());
		editor.setOpaque(true);
		editor.setHorizontalAlignment(JTextField.CENTER);
		chart.add(editor);
		chart.addComponentListener(this);
		editor.grabFocus();
	}
public void stopEdit() {
		chart.remove(editor);
		chart.getTranslator().setText(task, editor.getText());
		chart.repaint(editor.getBounds());
		chart.removeComponentListener(this);
		
		editor = null;
		task = null;
		
		chart.fireChangeEvent();
	}
	
	@Override
	public void mousePressed(MouseEvent e) {
		if ((editor != null) && (task != null)) {
			stopEdit();
			e.consume();
		}
	}
	
	@Override
	public void mouseClicked(MouseEvent e) {
		if (e.isConsumed()) {
			return;
		}
		
		if ((e.getButton() == MouseEvent.BUTTON1) && (e.getClickCount() == 2)) {
			Object task = chart.getTaskAtPoint(e.getPoint());
			
			if (task != null) {
				startEdit(task);
				e.consume();
			}
		}
	}
	
	@Override
	public void mouseMoved(MouseEvent e) {
		if (task != null) {
			chart.setCursor(Cursor.getDefaultCursor());
			e.consume();
		}
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if ((editor != null) && (task != null)) {
			stopEdit();
		}
	}
	
	@Override
	public void focusGained(FocusEvent e) {
}
	
	@Override
	public void focusLost(FocusEvent e) {
		if ((editor != null) && (task != null)) {
			stopEdit();
		}
	}

	@Override
	public void componentHidden(ComponentEvent e) {
		if ((editor != null) && (task != null)) {
			stopEdit();
		}
	}

	@Override
	public void componentMoved(ComponentEvent e) {
		if ((editor != null) && (task != null)) {
			Rectangle2D bounds = chart.getTaskBounds(task);
			editor.setBounds(bounds.getBounds());
		}
	}

	@Override
	public void componentResized(ComponentEvent e) {
		if ((editor != null) && (task != null)) {
			Rectangle2D bounds = chart.getTaskBounds(task);
			editor.setBounds(bounds.getBounds());
		}
	}

	@Override
	public void componentShown(ComponentEvent e) {
		if ((editor != null) && (task != null)) {
			stopEdit();
		}
	}
	
}