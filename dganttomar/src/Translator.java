package dganttomar;

import java.util.Date;


public abstract class Translator {
	
	
	public Translator() {
		super();
	}
	
	
	public abstract int getRow(Object task);
	
	
	public abstract long getStart(Object task);
	
	
	public abstract long getEnd(Object task);
	
	
	public abstract String getText(Object task);
	
	
	public String getToolTipText(Object task) {
		return null;
	}
	
	
	public void setRow(Object task, int row) {
		
	}

	
	public void setStart(Object task, long start) {
		
	}
	
	
	public void setEnd(Object task, long end) {
		
	}

	
	public void setText(Object task, String text) {
		
	}

}