package dganttomar;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;
import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.table.AbstractTableModel;
import javax.swing.undo.UndoManager;


public class Example {
	

	
	private static class Task {

		public String id;
		public Date start;
		public Date end;

		public Task(String id, Date start, Date end) {
			super();
			this.id = id;
			this.start = start;
			this.end = end;
		}

	}
	
	
	private static class TaskTranslator extends Translator {

		private final List<Task> modules;

		public TaskTranslator(List<Task> modules) {
			super();
			this.modules = modules;
		}

		@Override
		public long getStart(Object task) {
			return ((Task)task).start.getTime();
		}

		@Override
		public long getEnd(Object task) {
			return ((Task)task).end.getTime();
		}

		@Override
		public String getText(Object task) {
			return ((Task)task).id;
		}

		@Override
		public String getToolTipText(Object task) {
			return ((Task)task).id;
		}

		@Override
		public void setStart(Object task, long start) {
			Date startDate = new Date(start);
			((Task)task).start = startDate;
		}
		
		@Override
		public void setEnd(Object task, long end) {
			Date endDate = new Date(end);
			((Task)task).end = endDate;
		}

		@Override
		public void setText(Object task, String text) {
			((Task)task).id = text;
		}

		@Override
		public int getRow(Object task) {
			return modules.indexOf(task);
		}

	}

	
	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException | IllegalAccessException | InstantiationException | UnsupportedLookAndFeelException e) {
			
		}

		
		Calendar calendar = Calendar.getInstance();
		Random random = new Random();
		final List<Task> tasks = new ArrayList<>();

		for (int i = 0; i < 14; i++) {
			calendar.add(Calendar.DATE, -random.nextInt(5));

			Date start = calendar.getTime();

			calendar.add(Calendar.DATE, random.nextInt(4) + 2);

			Date end = calendar.getTime();

			tasks.add(new Task("Tache " + (i + 1), start, end));
		}

		
		TaskTranslator translator = new TaskTranslator(tasks);

		
		GanttModel dataModel = new BasicGanttModel(tasks);

		
		BasicLinkModel linkModel = new BasicLinkModel();
		linkModel.addLink(tasks.get(0), tasks.get(1), LinkType.FINISH_TO_START);
		linkModel.addLink(tasks.get(2), tasks.get(3), LinkType.FINISH_TO_START);
		linkModel
				.addLink(tasks.get(4), tasks.get(5), LinkType.FINISH_TO_START);

		linkModel.addLink(tasks.get(10), tasks.get(11),
				LinkType.FINISH_TO_START);
		linkModel.addLink(tasks.get(10), tasks.get(12),
				LinkType.FINISH_TO_START);
		linkModel.addLink(tasks.get(10), tasks.get(13),
				LinkType.FINISH_TO_START);

		
		final GanttChart chart = new GanttChart(dataModel, translator, linkModel);

		
		BasicSelectionHandler boxSelectionHandler = new BasicSelectionHandler(chart);
		chart.addMouseListener(boxSelectionHandler);
		chart.addMouseMotionListener(boxSelectionHandler);

		
		BasicLinkEditorHandler linkHandler = new BasicLinkEditorHandler(chart);
		chart.addMouseListener(linkHandler);
		chart.addMouseMotionListener(linkHandler);

		
		BasicZoomHandler zoomHandler = new BasicZoomHandler(chart);
		chart.addMouseListener(zoomHandler);
		chart.addMouseMotionListener(zoomHandler);

		
		BasicDoubleClickHandler doubleClickHandler = new BasicDoubleClickHandler(chart);
		chart.addMouseListener(doubleClickHandler);
		chart.addMouseMotionListener(doubleClickHandler);

		
		BasicTaskEditorHandler taskEditorHandler = new AdvancedTaskEditorHandler(chart, new UndoManager());
		chart.addMouseListener(taskEditorHandler);
		chart.addMouseMotionListener(taskEditorHandler);

		
		final GanttHeader header = new GanttHeader(chart);

		
		final GanttTable table = new GanttTable(chart, header,
				new AbstractTableModel() {

					private static final long serialVersionUID = 5721139181257247921L;

					@Override
					public int getColumnCount() {
						return 1;
					}

					@Override
					public int getRowCount() {
						return tasks.size();
					}

					@Override
					public Object getValueAt(int rowIndex, int columnIndex) {
						return tasks.get(rowIndex).id;
					}

					@Override
					public String getColumnName(int column) {
						return "Tasks";
					}

				});

		
		GanttPanel panel = new GanttPanel(chart, header, table);

		
		final JFrame frame = new JFrame("Gantt omar mezvip");
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.setSize(400, 400);

		JPanel buttonPane = new JPanel(new FlowLayout(FlowLayout.RIGHT));

		buttonPane.add(new JButton(new AbstractAction("Close") {

			private static final long serialVersionUID = 3805426937056229358L;

			@Override
			public void actionPerformed(ActionEvent e) {
				frame.dispose();
			}

		}));

		frame.getContentPane().add(panel, BorderLayout.CENTER);
		frame.getContentPane().add(buttonPane, BorderLayout.SOUTH);
		frame.setVisible(true);
	}

}