package dganttomar;

import java.util.ArrayList;
import java.util.List;

import javax.swing.undo.AbstractUndoableEdit;
import javax.swing.undo.UndoManager;
import javax.swing.undo.UndoableEdit;


public class GanttChartUndoableEdit extends AbstractUndoableEdit {

	private static final long serialVersionUID = 1177932236828804040L;

	
	public static final int KEYBOARD = 0;
	
	
	public static final int MOUSE = 1;

	
	private int type;
	
	
	private final UndoManager undoManager;
	
	
	private final GanttChart chart;

	
	private List<Object> editedTasks;
	
	
	private List<State> beforeState;
	
	
	private List<State> afterState;


	public GanttChartUndoableEdit(int type, GanttChart chart,
			UndoManager undoManager) {
		super();
		this.type = type;
		this.chart = chart;
		this.undoManager = undoManager;

		editedTasks = new ArrayList<Object>();
	}

	@Override
	public void undo() {
		for (State state : beforeState) {
			Translator translator = chart.getTranslator();
			translator.setStart(state.getTask(), state.getStart());
			translator.setEnd(state.getTask(), state.getEnd());
			translator.setRow(state.getTask(), state.getRow());
		}
		
		chart.fireChangeEvent();
		chart.repaint();
	}

	@Override
	public void redo() {
		for (State state : afterState) {
			chart.getTranslator().setStart(state.getTask(), state.getStart());
			chart.getTranslator().setEnd(state.getTask(), state.getEnd());
			chart.getTranslator().setRow(state.getTask(), state.getRow());
		}
		
		chart.fireChangeEvent();
		chart.repaint();
	}

	@Override
	public boolean canRedo() {
		return true;
	}

	
	public void addEditedTask(Object task) {
		editedTasks.add(task);
	}

	
	public void grabBeforeSnapshot() {
		beforeState = new ArrayList<State>();
		
		for (Object task : editedTasks) {
			beforeState.add(new State(task, 
					chart.getTranslator().getStart(task), 
					chart.getTranslator().getEnd(task), 
					chart.getTranslator().getRow(task)));
		}
	}

	
	public void grabAfterSnapshot() {
		afterState = new ArrayList<State>();
		
		for (Object task : editedTasks) {
			afterState.add(new State(task,
					chart.getTranslator().getStart(task), 
					chart.getTranslator().getEnd(task), 
					chart.getTranslator().getRow(task)));
		}
	}

	@Override
	public boolean addEdit(UndoableEdit edit) {
		if (edit instanceof GanttChartUndoableEdit) {
			GanttChartUndoableEdit newedit = (GanttChartUndoableEdit)edit;
		
			if ((type == KEYBOARD) && (newedit.type == KEYBOARD)) {
				if (editedTasks.equals(newedit.editedTasks)) {
					afterState = newedit.afterState;
					return true;
				} else {
					return false;
				}
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	
	public void commit() {
		if ((beforeState == null) || (afterState == null)) {
			throw new IllegalStateException("incomplete edit");
		}

		undoManager.addEdit(this);
		
		beforeState = null;
		afterState = null;
	}

}