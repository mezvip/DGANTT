package dganttomar;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;

import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableModel;


public class GanttTable extends JTable implements TableCellRenderer {
	
	private static final long serialVersionUID = 2597593882834133527L;
	
	
	private final GanttChart chart;
	
	
	private final GanttHeader header;

	
	public GanttTable(GanttChart chart, GanttHeader header, 
			TableModel tableModel) {
		super(tableModel);
		this.chart = chart;
		this.header = header;
		
		setRowHeight(chart.getRowHeight());
		
		for (int i=0; i<getColumnModel().getColumnCount(); i++) {
			getColumnModel().getColumn(i).setHeaderRenderer(this);
			getColumnModel().getColumn(i).setCellRenderer(this);
		}

		setShowGrid(false);
		setIntercellSpacing(new Dimension(0, 0));
		setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
		setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
	}
	
	@Override
	public JComponent getTableCellRendererComponent(final JTable table, 
			Object value, boolean isSelected, boolean hasFocus, final int row, 
			int column) {
		JLabel label = new JLabel(value.toString()) {

			private static final long serialVersionUID = -1857424941970648741L;

			@Override
			protected void paintComponent(Graphics g) {
				if (row >= 0) {
					chart.getRowRenderer().paintRow(g, chart, row, 
							g.getClipBounds(), true);
				}
				
				super.paintComponent(g);
			}
			
		};
		
		if (row < 0) {
			label.setOpaque(true);
			label.setBackground(header.getBackground());
			label.setPreferredSize(new Dimension(1, 
					header.getPreferredSize().height));
			label.setBorder(BorderFactory.createCompoundBorder(
					BorderFactory.createMatteBorder(0, 1, 1, 0, Color.GRAY),
					BorderFactory.createEmptyBorder(0, 2, 0, 0)));
		} else {
			label.setBorder(BorderFactory.createCompoundBorder(
					BorderFactory.createMatteBorder(0, 1, 0, 0, Color.GRAY),
					BorderFactory.createEmptyBorder(0, 2, 0, 0)));
		}
		
		return label;
	}

}