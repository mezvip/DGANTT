package dganttomar;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.awt.geom.Rectangle2D;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.Vector;

import javax.swing.JComponent;
import javax.swing.JScrollPane;
import javax.swing.JViewport;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;


public class GanttChart extends JComponent implements GanttModelListener {
	
	private static final long serialVersionUID = 1745970251668888420L;
	
	
	private final GanttModel model;
	
	
	private final Translator translator;
	
	
	private final LinkModel linkModel;
	
	
	private RowRenderer rowRenderer;


	private TaskRenderer taskRenderer;
	
	
	private LinkRenderer linkRenderer;
	
	
	private Set<Object> selectedTasks;
	
	
	private List<GanttSelectionListener> selectionListeners;
	
	
	private List<ChangeListener> changeListeners;
	
	
	private double zoom;
	
	
	protected long rangeMinimum;
	
	
	protected long rangeMaximum;

	
	private int rowHeight;
	

	private Insets rowInsets;
	

	public GanttChart(GanttModel model, Translator translator) {
		this(model, translator, null);
	}
	

	public GanttChart(GanttModel model, Translator translator, 
			LinkModel linkModel) {
		super();
		this.model = model;
		this.translator = translator;
		this.linkModel = linkModel;
		
		rowRenderer = new BasicRowRenderer();
		taskRenderer = new BasicTaskRenderer();
		linkRenderer = new BasicLinkRenderer();
		
		selectedTasks = new HashSet<Object>();
		selectionListeners = new Vector<GanttSelectionListener>();
		changeListeners = new Vector<ChangeListener>();

		rowHeight = 20;
		rowInsets = new Insets(1, 1, 1, 1);
		zoom = 1.0;
		
		computeRange();
		setToolTipText("");
		
		model.addGanttModelListener(this);
	}


	public void addGanttSelectionListener(GanttSelectionListener listener) {
		selectionListeners.add(listener);
	}
	

	public void removeGanttSelectionListener(GanttSelectionListener listener) {
		selectionListeners.remove(listener);
	}
	

	public void addChangeListener(ChangeListener listener) {
		changeListeners.add(listener);
	}
	

	public void removeChangeListener(ChangeListener listener) {
		changeListeners.remove(listener);
	}
	

	public void fireSelectionEvent() {
		GanttSelectionEvent event = new GanttSelectionEvent(this);
		
		for (GanttSelectionListener listener : selectionListeners) {
			listener.valueChanged(event);
		}
	}
	

	public void fireChangeEvent() {
		ChangeEvent event = new ChangeEvent(this);
		
		for (ChangeListener listener : changeListeners) {
			listener.stateChanged(event);
		}
	}


	public void selectTask(Object task) {
		selectedTasks.add(task);
		fireSelectionEvent();
	}
	

	public void unselectTask(Object task) {
		selectedTasks.remove(task);
		fireSelectionEvent();
	}
	

	public void toggleTaskSelection(Object task) {
		if (selectedTasks.contains(task)) {
			selectedTasks.remove(task);
		} else {
			selectedTasks.add(task);
		}
		
		fireSelectionEvent();
	}
	

	public boolean isTaskSelected(Object task) {
		return selectedTasks.contains(task);
	}
	

	public Set<Object> getSelectedTasks() {
		return selectedTasks;
	}
	

	public Object getSelectedTask() {
		if (selectedTasks.isEmpty()) {
			return null;
		}
		
		return selectedTasks.iterator().next();
	}
	

	public void clearSelection() {
		selectedTasks.clear();
		fireSelectionEvent();
	}
	

	public void resize() {
		long oldMinimum = rangeMinimum;
		long oldMaximum = rangeMaximum;
		
		computeRange();

		if ((oldMinimum != rangeMinimum) || (oldMaximum != rangeMaximum)) {
			forceRevalidateAndRepaint();
		}
	}
	

	protected void forceRevalidateAndRepaint() {
		revalidate();
		repaint();
		
		if (getParent() instanceof JViewport) {
			JViewport viewport = (JViewport)getParent();
			viewport.revalidate();
			viewport.repaint();
			
			if (viewport.getParent() instanceof JScrollPane) {
				JScrollPane scrollPane = (JScrollPane)viewport.getParent();
				scrollPane.revalidate();
				scrollPane.repaint();
			}
		}
	}
	

	private void computeRange() {
		rangeMinimum = Long.MAX_VALUE;
		rangeMaximum = Long.MIN_VALUE;
		
		for (int i=0; i<model.getTaskCount(); i++) {
			Object task = model.getTaskAt(i);
			
			rangeMinimum = Math.min(rangeMinimum, translator.getStart(task));
			rangeMaximum = Math.max(rangeMaximum, translator.getEnd(task));
		}
		
		if ((rangeMinimum == Long.MAX_VALUE) || (rangeMaximum == Long.MIN_VALUE)) {
			//model empty
			rangeMinimum = 0;
			rangeMaximum = 0;
		}
	}
	
	@Override
	public Dimension getPreferredSize() {
		int width;
		
		if (getParent() instanceof JViewport) {
			width = (int)(zoom * getParent().getWidth());
		} else {
			width = super.getPreferredSize().width;
		}
		
		int height = model.getRowCount() * getRowHeight();

		return new Dimension(width, height);
	}


	public Integer getRow(double y) {
		if (y < 0)
			return null;
		
		if (y > getHeight())
			return null;
		
		int row = (int)(y / getRowHeight());
		
		if (row >= model.getRowCount())
			return null;

		return row;
	}
	

	private double getScale() {
		return (double)(getWidth() - rowInsets.left - rowInsets.right) /
				(double)(rangeMaximum - rangeMinimum);
	}
	

	public double canonicalToScreen(long value) {
		return getScale()*(value - rangeMinimum) + rowInsets.left;
	}
	

	public long screenToCanonical(double x) {
		return (long)((x - rowInsets.left) / getScale()) + rangeMinimum;
	}
	

	public Rectangle2D getTaskBounds(Object task) {
		int row = translator.getRow(task);
		double start = canonicalToScreen(translator.getStart(task));
		double end = canonicalToScreen(translator.getEnd(task));
		double top = row*getRowHeight() + rowInsets.top;
		
		return new Rectangle2D.Double(start, top, end - start, rowHeight);
	}
	

	public Rectangle2D getTaskBounds(Object task, int row, double dstart, 
			double dwidth) {
		double start = canonicalToScreen(translator.getStart(task));
		double end = canonicalToScreen(translator.getEnd(task));
		double top = row*getRowHeight() + rowInsets.top;
		
		return new Rectangle2D.Double(start + dstart, top, end - start + dwidth,
				rowHeight);
	}
	
	
	public Rectangle2D getRowBounds(int row) {		
		return new Rectangle2D.Double(0.0, row*getRowHeight(), getWidth(),
				getRowHeight());
	}
	
	@Override
	public void paintComponent(Graphics g) {
		Graphics2D g2 = (Graphics2D)g;
		Rectangle clip = g2.getClipBounds();
		
		rowRenderer.paintBackground(g, this);
		
		for (int i=0; i<getModel().getRowCount(); i++) {
			Rectangle2D bounds = getRowBounds(i);
			
			if (bounds.intersects(clip)) {
				rowRenderer.paintRow(g, this, i, bounds, false);
			}
		}
			
		for (int i=0; i<model.getTaskCount(); i++) {
			Object task = model.getTaskAt(i);
			Rectangle2D bounds = getTaskBounds(task);
			
			if (bounds.intersects(clip)) {
				taskRenderer.paintTask(g, this, task, bounds, isTaskSelected(task));
			}
		}
		
		if ((linkRenderer != null) && (linkModel != null)) {
			for (int i=0; i<linkModel.getLinkCount(); i++) {
				linkRenderer.paintLink(g, this, linkModel.getLinkAt(i));
			}
		}
	}

	@Override
	public void ganttModelChanged(GanttModelEvent event) {
		//check to see if selected tasks still exist
		Iterator<Object> iterator = selectedTasks.iterator();
		boolean selectionChanged = false;
		
		while (iterator.hasNext()) {
			Object task = iterator.next();
			boolean found = false;
			
			for (int i=0; i<model.getTaskCount(); i++) {
				if (model.getTaskAt(i).equals(task)) {
					found = true;
					break;
				}
			}
			
			if (!found) {
				iterator.remove();
				selectionChanged = true;
			}
		}
		
		computeRange();
		forceRevalidateAndRepaint();
		fireChangeEvent();
		
		if (selectionChanged) {
			fireSelectionEvent();
		}
	}
	
	public Object getTaskAtPoint(Point point) {
		
		for (Object task : selectedTasks) {
			Rectangle2D bounds = getTaskBounds(task);
			
			if (bounds.contains(point)) {
				return task;			
			}
		}
		
		for (int i=model.getTaskCount()-1; i>=0; i--) {
			Object task = model.getTaskAt(i);
			Rectangle2D bounds = getTaskBounds(task);
			
			if (bounds.contains(point)) {
				return task;
			}
		}
		
		return null;	
	}
	
	@Override
	public String getToolTipText(MouseEvent e) {
		Object task = getTaskAtPoint(e.getPoint());
		
		if (task == null) {
			return null;
		} else {
			return translator.getToolTipText(task);
		}
	}
	

	public void setZoom(double zoom) {
		this.zoom = zoom;

		setSize(getPreferredSize());
		forceRevalidateAndRepaint();
	}
	

	public double getZoom() {
		return zoom;
	}


	public GanttModel getModel() {
		return model;
	}


	public Translator getTranslator() {
		return translator;
	}


	public LinkModel getLinkModel() {
		return linkModel;
	}


	public RowRenderer getRowRenderer() {
		return rowRenderer;
	}


	public void setRowRenderer(RowRenderer rowRenderer) {
		this.rowRenderer = rowRenderer;
	}


	public TaskRenderer getTaskRenderer() {
		return taskRenderer;
	}

	
	public void setTaskRenderer(TaskRenderer taskRenderer) {
		this.taskRenderer = taskRenderer;
	}

	
	public LinkRenderer getLinkRenderer() {
		return linkRenderer;
	}

	
	public void setLinkRenderer(LinkRenderer linkRenderer) {
		this.linkRenderer = linkRenderer;
	}

	
	public int getRowHeight() {
		return rowInsets.top + rowHeight + rowInsets.bottom;
	}

}